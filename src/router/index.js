import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [

  {
    path: '/',
    redirect: {
      path: '/login',
    }
  },
  {
    path: '/enrolls',
    name: "BaseEnrolls",
    component: () => import("@/components/index-component/BaseEnrolls")
  },
  {
    path: '/BaseKnow/:id',
    name: "BaseKnow",
    props: true,
    component: () => import("@/components/index-component/BaseKnow")
  },
  {
    path: '/wp',
    name: "wp",
    component: () => import("@/components/index-component/wp"),
    children:[

      {
        path: '/recallMultiple',
        name: "recallMultiple",
        component: () => import("@/components/form-list/form-recallMultiple")
      },
      {
        path: '/recallRecordForm',
        name: "recallRecordForm",
        component: () => import("@/components/form-list/form-recallRecordForm")
      },
      {
        path: '/recallLogout',
        name: "recallLogout",
        component: () => import("@/components/form-list/form-recallLogout")
      },
      {
        path: '/recallLogin',
        name: "recallLogin",
        component: () => import("@/components//form-list/form-recallLogin")
      },
      {
        path: '/recallApplyFor',
        name: "recallApplyFor",
        component: () => import("@/components/form-list/form-recallApplyFor")
      }
    ]
  },

  {
    path: '/login',
    name: "login",
    meta: {
      //判断是否是注册页面跳转到登录页面
      judegLogin: true
    },
    component: () => import("@/components/index-component/BaseLogin")
  },

  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: {
      isLogin: true
    },
    redirect: {
      path: '/IndexRightCont',
      meta: {
        isLogin: true
      },
    },
    children: [{
        path: '/IndexRightCont',
        name: "IndexRightCont",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/index-component/IndexRightCont")
      },
      {
        path: '/completeinfo',
        name: "completeinfo",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/verify-infor/complete-info")
      },
      {
        path: '/infor',
        name: "infor",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/verify-infor/infor")
      },
      {
        path: '/prepare',
        name: "prepare",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/verify-infor/prepare")
      },
      {
        path: '/register',
        name: "register",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/verify-infor/register")
      },
      {
        path: '/reminder',
        name: "reminder",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/verify-infor/reminder")
      },
      {
        path: '/register-apply',
        name: "register-apply",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/apply-for/register-apply")
      },
      {
        path: '/administer',
        name: "administer",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/apply-for/administer")
      },
      {
        path: '/form-exclusive',
        name: "form-exclusive",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/form-exclusive")
      },
      {
        path: '/form-login',
        name: "form-login",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/form-login")
      },
      {
        path: '/form-alteration',
        name: "form-alteration",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/form-alteration")
      },
      {
        path: '/form-extension',
        name: "form-extension",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/form-extension")
      },
      {
        path: '/form-assignmen',
        name: "form-assignmen",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/form-assignmen")
      },
<<<<<<< HEAD
  
      { 
        path: '/form-correct',
        name: "form-correct",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/form-correct")
      },
      
      { 
        path: '/form-permission',
        name: "form-permission",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/form-permission")
      },
      { 
        path: '/form-withdraw',
        name: "form-withdraw",
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/form-withdraw")
=======
      {
        path: '/delete-goods',
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/delete-goods")
      },
      {
        path: '/brand-logout',
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/brand-logout")
      },
      {
        path: '/priority-prove',
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/priority-prove")
      },
      {
        path: '/revocation-shift-apply',
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/revocation-shift-apply")
      },
      {
        path: '/termination-reference',
        meta: {
          isLogin: true
        },
        component: () => import("@/components/form-list/termination-reference ")
>>>>>>> 8079e6795f3eb586dd16dd76917e4b82d5273aeb
      },
    ]
  },

  // {
  //   path: '/guanil',
  //   name: 'guanil',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: function () {
  //     return import(/* webpackChunkName: "about" */ '../views/guanil.vue')

  //   }
  // }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})


// 页面登录的路由守卫
router.beforeEach((to, from, next) => {
  if (to.meta.isLogin && window.localStorage.getItem('token') === null) {

    Vue.prototype.$message({
      message: '您还没有登录',
      type: 'error',
      duration: 4000
    });
    next({
      path: '/login'
    })
  } else {
    next()
  }
})


export default router