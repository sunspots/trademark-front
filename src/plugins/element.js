import Vue from 'vue'
import 'element-ui/lib/theme-chalk/index.css'
import {
    Button,
    Row,
    Col,
    RadioGroup,
    MenuItem,
    Table,
    TableColumn,
    Menu,
    Form,
    FormItem,
    Input,
    Select,
    Option,
    Main,
    PageHeader,
    Header,
    Aside,
    Container,
    Upload,
    Dialog,
    Image,
    Checkbox,
    Radio,
    Pagination,
    Message,
    MessageBox,
    CheckboxButton,
    CheckboxGroup,
    DatePicker,
    Notification
} from 'element-ui'
Vue.prototype.$message = Message;
Vue.use(Button);
Vue.use(Row);
Vue.use(Col);
Vue.use(RadioGroup);
Vue.use(MenuItem);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Menu);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Input);
Vue.use(Select);
Vue.use(Option);
Vue.use(Main);
Vue.use(PageHeader);
Vue.use(Header);
Vue.use(Container);
Vue.use(Aside);
Vue.use(Upload);
Vue.use(Dialog);
Vue.use(Image);
Vue.use(Checkbox);
Vue.use(Radio);
Vue.use(Pagination);
Vue.use(CheckboxButton);
Vue.use(CheckboxGroup);
Vue.use(DatePicker);
Vue.prototype.$message = Message;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$notify = Notification;